--------------------------------------------------------------------------------
## Yocto Setup Guide

  1. Download the script "yocto-setup" at https://bitbucket.org/ric-cbm/repo-cbm-yocto
  2. Type: chmod +x yocto-setup
  3. Type: yocto-setup
  4. Wait for repo to download the yocto related repositories
  5. Yocto build environment is now at ~/poky



--------------------------------------------------------------------------------
## Yocto Build
  
  1. Type: source ~/poky/oe-init-build-env
  2. Type: export MACHINE="${MY_MACHINE}", Where:
    * ${MY_MACHINE} is "omap3logic" (default) for logicpd or "overo" for gumstix
  3. Type: bitbake cbm-min-sysvinit-console-image
  4. Wait for image to build (first time will take several hours)
  5. Images are at "~/poky/build/tmp/deploy/images

